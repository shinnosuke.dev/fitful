package com.fitful.fitfulgym.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitfulGymApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FitfulGymApiApplication.class, args);
    }

}
